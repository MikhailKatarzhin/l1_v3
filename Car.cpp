#include <iostream>
#include "Car.h"
#include <string>
#include <cstring>

using namespace std;
    Car :: Car()
    {
        cout << "\nConstruction... " << this;
    }
    Car :: Car(Car &otherCar)
    {
        this -> model = otherCar.model;
        this -> speed = otherCar.speed;
        this -> color = otherCar.color;
        cout << "\nConstruction copy... " << this;
    }
    void Car :: printInformationAboutCar()
    {
        cout << "Model: " << model << "\tSpeed: " << speed << "\tColor: " << color << endl;
    }

    void Car :: setModel(string model_in)
    {
        model = model_in;
    }
    void Car :: setSpeed(short speed_in)
    {
        speed = speed_in;
    }

    void Car :: setColor(string color_in)
    {
        color = color_in;
    }
    string Car :: getModel()
    {
        return model;
    }
    short Car :: getSpeed()
    {
        return speed;
    }

    string Car :: getColor()
    {
        return color;
    }

    Car :: ~Car()
    {
        cout << "\nDestruction... " << this;
    }

