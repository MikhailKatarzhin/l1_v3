#include <string>

using namespace std;
class Car
{
private:
    string model;
    short speed;
    string color;
public:
    Car();
    Car(const Car &car);
    void printInformationAboutCar();
    void setModel(string model_in);
    void setSpeed(short speed_in);
    void setColor(string color_in);
    string getModel();
    short getSpeed();
    string getColor();
    ~Car();
};
