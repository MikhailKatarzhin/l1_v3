#include <iostream>
#include "Car.h"
#include <string>
#include <stdlib.h>


using namespace std;


void showMenu()
{
    cout << "\t\t\t***Menu***" << endl;
    cout << "\t0)Menu up" << endl;
    cout << "\t1)Show all information about car" << endl;
    cout << "\t2)Set model of car" << endl;
    cout << "\t3)Set speed of car" << endl;
    cout << "\t4)Set color of car" << endl;
    cout << "\t5)Get model of car" << endl;
    cout << "\t6)Get speed of car" << endl;
    cout << "\t7)Get color of car" << endl;
    cout << "\t8)Copy this car" << endl;
}

void showAllCars(short countOfCar, Car *Cars)
{
    for (short i = 0; i < countOfCar; i++)
    {
        cout << i+1 << ")";
        Cars[i].printInformationAboutCar();
    }
}

int main()
{
    string tmpModel, tmpColor;
    short var = 0, tmpspeed = 0, countOfCar = 1, i = 0, y = 0;
    Car *Cars = new Car[1], *tmpCars;
    cout << "\t\t\t***Car's database***\n\n";
    while (1)
    {
        cout << "\nEntry number of car(1-" << countOfCar << ") or close program(input 0)\n" << endl;
        showAllCars(countOfCar, Cars);
        cin >> i;
        if (i == 0)
        {
            delete Cars;
            return 0;
        }

        while (i != 0)
        {
            showMenu();
            cin >> var;
            switch (var)
            {
                case 0:
                    i = 0;
                    break;
                case 1:
                    Cars[i-1].printInformationAboutCar();
                    break;
                case 2:
                    cout << "\nEntry model of car:" << endl;
                    cin >> tmpModel;
                    Cars[i-1].setModel(tmpModel);
                    break;
                case 3:
                    cout << "\nEntry speed of car:" << endl;
                    cin >> tmpspeed;
                    if (tmpspeed >= 0)
                    {
                        Cars[i-1].setSpeed(tmpspeed);
                    }else{
                        cout << "\nUncorrect speed set. Repeat." << endl;
                        }
                    break;
                case 4:
                    cout << "\nEntry color of car:" << endl;
                    cin >> tmpColor;
                    Cars[i-1].setColor(tmpColor);
                    break;
                case 5:
                    cout << "\nTaken model of car:\t" << (tmpModel = Cars[i-1].getModel()) << endl;
                    break;
                case 6:
                    cout << "\nTaken speed of car:\t" << (tmpspeed = Cars[i-1].getSpeed()) << endl;
                    break;
                case 7:
                    cout << "\nTaken color of car:\t" << (tmpColor = Cars[i-1].getColor()) << endl;
                    break;
                case 8:
                    countOfCar++;
                    cout << "There";
                    tmpCars = new Car[countOfCar];
                    for (y = 0; y < countOfCar-1; y++)
                    {
                        tmpCars[y] = Cars[y];
                        delete &(Cars[y]);
                    }
                    delete Cars;
                    Cars = tmpCars;
                    Car newCar(Cars[i-1]);
                    cout << "\n" << countOfCar;
                    Cars[countOfCar-1] = newCar;
                    break;
            }
        }
    }
}
